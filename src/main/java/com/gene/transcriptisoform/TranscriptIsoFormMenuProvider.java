package com.gene.transcriptisoform;

import aQute.bnd.annotation.component.Activate;
import aQute.bnd.annotation.component.Component;
import aQute.bnd.annotation.component.Deactivate;
import aQute.bnd.annotation.component.Reference;
import com.affymetrix.igb.swing.JRPCheckBoxMenuItem;
import com.affymetrix.igb.swing.JRPMenu;
import com.affymetrix.igb.swing.JRPMenuItem;
import com.affymetrix.igb.swing.JRPRadioButtonMenuItem;
import com.affymetrix.igb.swing.MenuUtil;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.ButtonGroup;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import org.lorainelab.igb.services.IgbService;

/**
 *
 * @author dcnorris
 */
@Component(name = TranscriptIsoFormMenuProvider.COMPONENT_NAME, immediate = true)
public class TranscriptIsoFormMenuProvider {

    public static final String COMPONENT_NAME = "TranscriptIsoFormMenuProvider";
    private TranscriptIsoformEvidenceVisualizationManager tierListener;
    private IgbService igbService;
    JMenuBar menuBar;
    JMenu viewMenu;
    JRPMenu transcriptIsoformMenu;

    @Activate
    public void activate() {
        tierListener = new TranscriptIsoformEvidenceVisualizationManager(igbService);
        menuBar = (JMenuBar) igbService.getApplicationFrame().getJMenuBar();
        viewMenu = MenuUtil.getMenu(menuBar, "View");
        transcriptIsoformMenu = new JRPMenu("Transcript_Isoform_menu", "Transcript Isoform");
        final JRPMenuItem selectRefTiersMenuItem = new JRPMenuItem("Select_ref_tiers","Select reference tiers");
        selectRefTiersMenuItem.addActionListener(
                new ActionListener() {
            @SuppressWarnings({"unchecked", "rawtypes"})
            @Override
            public void actionPerformed(ActionEvent e) {
                tierListener.setRefSeqTiers((List) igbService.getSelectedTierGlyphs());//can't use generics on List
            }
        }
        );
        transcriptIsoformMenu.add(selectRefTiersMenuItem);
        JRPCheckBoxMenuItem unfoundMenuItem = new JRPCheckBoxMenuItem("show_unfound","Show unfound", true);
        unfoundMenuItem.addActionListener(
                new ActionListener() {
            @SuppressWarnings({"unchecked", "rawtypes"})
            @Override
            public void actionPerformed(ActionEvent e) {
                tierListener.setShowUnfound(unfoundMenuItem.isSelected());
            }
        }
        );
        transcriptIsoformMenu.add(unfoundMenuItem);
        
        final JRPMenu densityMenu = new JRPMenu("show_density","Show density");
        ButtonGroup group = new ButtonGroup();
        JRPRadioButtonMenuItem thicknessMenuItem = new JRPRadioButtonMenuItem("thickness","Thickness", true);
        thicknessMenuItem.addActionListener(
                new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tierListener.setShowDensityThickness();
            }
        }
        );
        densityMenu.add(thicknessMenuItem);
        group.add(thicknessMenuItem);
        JRPRadioButtonMenuItem transparencyMenuItem = new JRPRadioButtonMenuItem("transparency","Transparency", false);
        transparencyMenuItem.addActionListener(
                new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tierListener.setShowDensityTransparency();
            }
        }
        );
        densityMenu.add(transparencyMenuItem);
        group.add(transparencyMenuItem);
        JRPRadioButtonMenuItem brightnessMenuItem = new JRPRadioButtonMenuItem("brightness", "Brightness", false);
        brightnessMenuItem.addActionListener(
                new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tierListener.setShowDensityBrightness();
            }
        }
        );

        densityMenu.add(brightnessMenuItem);
        group.add(brightnessMenuItem);


        transcriptIsoformMenu.add(densityMenu);

        viewMenu.add(transcriptIsoformMenu);
    }

    @Reference(optional = false)
    public void setIgbService(IgbService igbService) {
        this.igbService = igbService;
    }

    @Deactivate
    public void deactivate() {
        tierListener.clearExonConnectorGlyphs();
        igbService.getSeqMap().updateWidget();
        viewMenu.remove(transcriptIsoformMenu);
    }

}
